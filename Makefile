build:
	git config --global user.email "orta+dangersystems@artsy.net"
	git config --global user.name "Danger.Systems"
	apt-get update -yqqq
	apt-get install -y nodejs
	gem install bundler
	bundle install
	export RUBYOPT='-E utf-8'
	bundle exec rake generate
	bundle install
	cd static
	bundle exec middleman build --clean
	cd ..
	mv static/build public
